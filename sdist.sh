#!/bin/bash
(
  echo "ChangeLog exported from Git by ${USER}@${HOSTNAME} at $(date --iso=s)"
  git log --pretty=tformat:"%d~ %h %ai %s" |
  sed "s|^ (|\n (|" |
  sed "s|)~|)\n\n~|" |
  sed "s|^~ ||" |
  sed "s|; |\n                                  |g"
  echo ""
  echo " (eof)"
) > ChangeLog
cabal sdist
