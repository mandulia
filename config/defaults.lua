--[[--

Mandulia -- Mandelbrot/Julia explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

--]]--

do

  local m = mandulia -- shorter aliased name for clarity

  -- print(unpack(m.args))    -- extra command line arguments are here

  -- items marked (*) take effect only at initialization time
  -- unmarked items can be changed at any time
  m.record      = false       --     dump frames as PPM to stdout
  m.fullscreen  = false       --     full screen mode
  m.width       = 1024        -- (*) window width
  m.height      =  576        -- (*) window height
  m.fps         =   25        --     frames per second
  m.detail      =   11        -- (*) detail level
  if m.width > m.height then  --     display size of each Julia set
    m.displaysize = m.width / 16
  else
    m.displaysize = m.height / 16
  end
  m.juliasize   =  256        -- (*) pixel size of each Julia set
  m.jobs        =  512        -- (*) maximum number of jobs per frame
  m.images      =  512        -- (*) maximum number of images to cache
  m.textures    = 2048        -- (*) maximum number of textures to cache
  m.workers     =    2        -- (*) number of worker threads
  m.view        = { x = 0, y = 0, z = 0 } -- viewport coordinates

  -- gets called when the window size changes
  function m.reshape(width, height)
    m.width  = width
    m.height = height
    if width > height then
      m.displaysize = width / 16
    else
      m.displaysize = height / 16
    end
  end

  -- gets called at the start of each frame
  -- eg: set 'view' here for animations
  function m.render()
  end

  -- gets called when a key is pressed
  -- eg: call 'quit()' if you want to quit
  function m.keyboard(key)
  end

  -- gets called when the program is exiting
  function m.atexit()
  end

end
