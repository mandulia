{-
Mandulia -- Mandelbrot/Julia explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module PriorityCache (PriorityCache(..), priorityCache, cache) where

import Sort

data PriorityCache c =
  PriorityCache
    { cSize :: Int
    , cContents :: [c]
    }

priorityCache :: Int -> PriorityCache c
priorityCache size = PriorityCache{ cSize = size, cContents = [] }

cache :: (c -> Double) -> [c] -> PriorityCache c -> IO (PriorityCache c, [c])
cache p xs c = do
  cs <- sortIO p (xs ++ cContents c)
  let (ys, zs) = splitAt (cSize c) cs
  return (c{ cContents = ys }, zs)
