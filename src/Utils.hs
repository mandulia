{-
Mandulia -- Mandelbrot/Julia explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module Utils (phi, phi', clamp, modifyIORef') where

import Data.IORef(IORef(), readIORef, writeIORef)

phi  :: Double
phi  = (sqrt 5 + 1) / 2

phi' :: Double
phi' = (sqrt 5 - 1) / 2

clamp :: Ord r => r -> r -> r -> r
clamp mi ma x = ma `min` x `max` mi

modifyIORef' :: IORef a -> (a -> a) -> IO ()
modifyIORef' r m = readIORef r >>= \v -> writeIORef r $! m v
