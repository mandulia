/*
Mandulia -- Mandelbrot/Julia explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include "qsort.h"

static int scmp(const void *x, const void *y) {
  const struct sortee *sx = x;
  const struct sortee *sy = y;
  double kx = sx->key;
  double ky = sy->key;
  if (kx < ky) return -1;
  if (kx > ky) return  1;
  return 0;
}

void sort(struct sortee *p, int n) {
  qsort(p, n, sizeof(struct sortee), scmp);
}
